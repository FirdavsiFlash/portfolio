export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'PortFolio',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [{
        charset: 'utf-8'
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1'
      },
      {
        hid: 'description',
        name: 'description',
        content: ''
      },
      {
        name: 'format-detection',
        content: 'telephone=no'
      }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: './icons8-f-32'
    }]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["aos/dist/aos.css"],
 


  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{
    src: '~/plugins/ScrollMagic.js',
    mode: 'client'
  }, {
    src: '~/plugins/aos.js',
    ssr: false
  }],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/i18n',
  ],
  i18n: {
    locales: ['en', 'ru'],
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'en',
      messages: {
        en: {
          begining: "Hi, Iam Firdavsiy",
          me: "A Nuxt-Vue .JS Developer Enthusiast",
          name: "Firdavsiy",
          secondName: "Maxmudov",
          about: "About me",
          portfolio: "Portfolio",
          skils: "Skils",
          contacts: "Contacts",
          fun: "Want to know more or just chat? You are welcome!",
          home: "Home",
          rest: "Send a message",
          stak: "My stack and the programs i work",
          getContacts: "Get contacts",
          likeMe: "Like me on",
          btnShow: "Show more",
          btnHide: "Hide all",
          myContacts: "My contacts",
          callMe: "Call from 08:00 to 23:00 Mon-Sat",
          index: {
            top: {
              about: {
                part1: "VUE | NUXT developer",
                part2: " 18 years old, Samarkand"
              }
            },
            about_page: {
              title: "About me",
              info: "Hi, I'm Firdavsiy – VUE/NUXT frontend developer from Samarakand. I'm interested in programming and everything connected with it.",
              secondary: "I finished at courses 'Frontend-programming' and 'Vue-Nuxt .JS' in Wepro it school",
              end: "Ready to implement excellent projects with wonderful people"
            },
            portfolio: {
              info: "There is some of my projects",
              project: "Go to project."
            }
          },
        },

        ru: {
          begining: "Привет, я Фирдавсий",
          me: "Nuxt-Vue .JS Pазработчик-Энтузиаст",
          name: "Фирдавсий",
          secondName: "Маxмудов",
          about: "Обо мне",
          portfolio: "Портфолио",
          skils: "Навыки",
          contacts: "Контакты",
          fun: "Хотите узнать больше или просто поболтать? Пожалуйста!)",
          home: "Главная",
          rest: "Отправить сообщение",
          stak: "Мой стек и программы, с которыми я работаю",
          getContacts: "Получить контакты",
          likeMe: "Найди меня в",
          btnShow: "Показать больше",
          btnHide: "Cкрыть",
          myContacts: "Mои контакты",
          callMe: "Звоните с 08:00 до 23:00 ПН-СБ",
          index: {
            top: {
              about: {
                part1: "VUE | NUXT-разработчик",
                part2: " 18 лет, город Самарканд"
              }
            },
            about_page: {
              title: "Обо мне",
              info: "Привет, я Фирдавсий — фронтенд-разработчик VUE/NUXT из Самараканда. Меня интересует программирование и все, что с ним связано.",
              secondary: "Закончил курсы 'Frontend-программирование' и 'Vue-Nuxt.JS' в Wepro it school",
              end: "Готовы реализовать отличные проекты с замечательными людьми!"
            },
            portfolio: {
              info: "Некоторые из моих проектов",
              project: "Подробно о проектe"
            }
          },
        },
      }
    }
  },

  target: 'static',

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ['gsap'],
    vendor: ["aos"],
  },
  theme: {
    container: {
      center: true,
    },
  },
  darkMode: 'class',
  server: {
    port: 7777,
  },
  publicRuntimeConfig: {
    baseURL: 'https://firdavsiy-portfolio-db.herokuapp.com/',
  },
}
