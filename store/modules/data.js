import Vuex from 'vuex'
import Vue from 'vue'
import api from '../../services/Routes'
// import store from '../index'


Vue.use(Vuex)

const state = () => ({
  AllData: {
    AllProjects: {
      data: [],
      title: "AllProjects",
      got: false
    },
  },
  ApiData: {
    AllProjects: {
      api: 'projects',
      methods: 'get'
    },
  }
})

const getters = {
  AllData: state => {
    return state.AllData
  },
}

const mutations = {
  GetData(state, data) {
    state.AllData[data.key].data = data.value
    state.AllData[data.key].got = true
    console.log(state.AllData[data.key]);
  },
  GetOneElement(state, data) {
    state.OneElement = data
  },
}

const actions = {
  GetData({
    commit,
    dispatch
  }, data) {
    for (let item of data) {
      api.get({
          api: state().ApiData[item].api
        })
        .then(res => {

          if (res.status == 200 || res.status == 201 || res.status == 202 || res.status == 203 || res.status == 204) {
            commit('GetData', {
              key: item,
              value: res.data.body
            })
          }
        })
        .catch(error => {
          console.error(`CANT GET ELEMENTS ${item}`)
        })
    }
  },
  SEND_LEADS({
    commit,
    dispatch
  }, what) {

    let obj = {}
    let fm = new FormData(event.target)

    let btn = event.target.querySelector("button")
    btn.classList.add("_btn_loading")

    fm.forEach((value, key) => {
      obj[key] = value
    })

    if (obj.alone) {
      obj.alone = true
    }

    api.post({
        api: state().AddLead[what].api,
        obj
      })
      .then(res => {
        btn.classList.remove("_btn_loading")
        commit('CLOSE_ALL_MODALS')
      })
      .catch(err => {
        btn.classList.remove("_btn_loading")
        console.log(err);
      })

    console.log(event.target);
  },
  GetOneElement({
    commit,
    dispatch
  }) {
    let element =
      window.location.href.split("/")[
        window.location.href.split("/").length - 1
      ];
    api.get({
        api: state().ApiData.OneElement.api + element
      })
      .then(res => {
        commit("GetOneElement", res.data)
      })
      .catch(err => console.log(err))
  }
}
const modules = {}
export default {
  state,
  getters,
  actions,
  mutations,
  modules
}
