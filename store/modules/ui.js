import Vue from 'vue'
import Vuex from 'vuex'
import index from '../index'

Vue.use(Vuex)

const state = () => ({
  modals: {
    contacts: {
      status: false,
      courseList: false
    },
    mobile_menu: {
      status: false,
    },
  },
})

const getters = {
  modals: state => state.modals,
}
const mutations = {
  CLOSE_ALL_MODALS(state) {
    for (let item in state.modals) {
      state.modals[item].status = false
    }
  },
  MANAGE_MODAL(state, details) {
    state.modals[details.name].status = !state.modals[details.name].status
    state.modals[details.name].text = details.text
  },
}

const actions = {

}

export default {
  state,
  getters,
  actions,
  mutations
}
