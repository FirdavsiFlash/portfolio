import Vuex from 'vuex'
import Vue from 'vue'
import ui from './modules/ui.js'
import GetData from './modules/data.js'
import vueSmoothScroll from 'vue-smooth-scroll'
Vue.use(vueSmoothScroll)

Vue.use(Vuex)

const state = () => ({

})

const getters = {
  ApiData: state => state.ApiData
}

const mutations = {

}

const actions = {

}

const modules = {
  ui,
  GetData
}

export default {
  state,
  getters,
  actions,
  mutations,
  modules
}
